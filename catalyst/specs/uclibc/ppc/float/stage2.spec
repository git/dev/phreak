subarch: ppc
target: stage2
version_stamp: uclibc-softfloat-2008.0_pre
rel_type: default
profile: uclibc/ppc
snapshot: 2008.0
source_subpath: default/stage1-ppc-uclibc-softfloat-2008.0_pre
cflags: -Os -pipe
cxxflags: -Os -pipe
ldflags: -Wl,-O1
chost: powerpc-softfloat-linux-uclibc
portage_confdir: /var/tmp/catalyst/portage_confdir/uclibc
portage_overlay: /var/tmp/catalyst/portage_overlay/uclibc
