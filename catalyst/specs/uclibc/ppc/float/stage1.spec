subarch: ppc
target: stage1
version_stamp: uclibc-softfloat-2008.0_pre
rel_type: default
profile: uclibc/ppc
snapshot: 2008.0
source_subpath: stage3-ppc-uclibc-softfloat-2007.0
cflags: -Os -pipe
cxxflags: -Os -pipe
ldflags: -Wl,-O1
chost: powerpc-softfloat-linux-uclibc
portage_confdir: /var/tmp/catalyst/portage_confdir/uclibc
portage_overlay: /var/tmp/catalyst/portage_overlay/uclibc
