subarch: x86
target: stage1
version_stamp: uclibc-2008.0_pre
rel_type: default
profile: uclibc/x86
snapshot: 2008.0
source_subpath: stage3-x86-uclibc-2007.0
cflags: -Os -mtune=i386 -pipe
cxxflags: -Os -mtune=i386 -pipe
ldflags: -Wl,-O1
chost: i386-gentoo-linux-uclibc
portage_confdir: /var/tmp/catalyst/portage_confdir/uclibc
portage_overlay: /var/tmp/catalyst/portage_overlay/uclibc
