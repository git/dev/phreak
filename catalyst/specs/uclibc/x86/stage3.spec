subarch: x86
target: stage3
version_stamp: uclibc-2008.0_pre
rel_type: default
profile: uclibc/x86
snapshot: 2008.0
source_subpath: default/stage2-x86-uclibc-2008.0_pre
cflags: -Os -mtune=i386
cxxflags: -Os -mtune=i386
ldflags: -Wl,-O1
portage_confdir: /var/tmp/catalyst/portage_confdir/uclibc
portage_overlay: /var/tmp/catalyst/portage_overlay/uclibc
