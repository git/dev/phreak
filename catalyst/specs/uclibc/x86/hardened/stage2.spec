subarch: x86
target: stage2
version_stamp: uclibc-hardened-2008.0_pre
rel_type: default
profile: uclibc/x86/hardened
snapshot: 2008.0
source_subpath: default/stage1-x86-uclibc-hardened-2008.0_pre
cflags: -Os -pipe
cxxflags: -Os -pipe
ldflags: -Wl,-O1
chost: i386-gentoo-linux-uclibc
portage_confdir: /var/tmp/catalyst/portage_confdir/uclibc
portage_overlay: /var/tmp/catalyst/portage_overlay/uclibc
